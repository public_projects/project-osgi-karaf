package org.green.osgi.karaf.a.client;

import org.green.osgi.karaf.a.api.ServiceA;
import org.green.osgi.karaf.c.api.InsertCService;
import java.util.ArrayList;

public class ClientServiceImpl implements ClientService {

    private ServiceA serviceA;
    private InsertCService insertCService;

    @Override
    public void setServiceA(ServiceA serviceA) {
        System.err.println("Setting ServiceA");
        this.serviceA = serviceA;
    }

    @Override
    public void setInsertCService(InsertCService insertCService) {
        System.err.println("Setting InsertCService");
        this.insertCService = insertCService;
    }

    @Override
    public void doService() {
        System.err.println("[doService]" + this.serviceA.getType());
        this.insertCService.insert(new ArrayList<>());
    }

    @Override
    public void endService() {
        System.err.println("[endService]" + this.serviceA.getType());
    }
}
