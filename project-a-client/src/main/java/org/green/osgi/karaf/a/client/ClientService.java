package org.green.osgi.karaf.a.client;

import org.green.osgi.karaf.a.api.ServiceA;
import org.green.osgi.karaf.c.api.InsertCService;

public interface ClientService {
    void setServiceA(ServiceA serviceA);
    void setInsertCService(InsertCService insertService);
    void doService();
    void endService();
}
