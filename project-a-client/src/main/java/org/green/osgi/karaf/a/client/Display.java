package org.green.osgi.karaf.a.client;

public class Display {
    private ClientService clientService;

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }

    public void init() {
        System.err.println("[Display][init] start ....");
        clientService.doService();
        System.err.println("[Display][init] end ....");
    }

    public void destroy() {
        System.err.println("[Display][destroy] start ....");
        clientService.endService();
        System.err.println("[Display][destroy] end ....");
    }
}
