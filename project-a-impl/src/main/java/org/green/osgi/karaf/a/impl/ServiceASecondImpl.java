package org.green.osgi.karaf.a.impl;

import org.green.osgi.karaf.a.api.ServiceA;

public class ServiceASecondImpl implements ServiceA {
    @Override
    public String getType() {
        return "This is service secondA";
    }
}
