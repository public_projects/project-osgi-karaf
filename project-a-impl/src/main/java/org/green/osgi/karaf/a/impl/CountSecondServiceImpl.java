package org.green.osgi.karaf.a.impl;

import org.green.osgi.karaf.a.api.CountService;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoField;

public class CountSecondServiceImpl implements CountService {
    @Override
    public long getCount() {
        System.err.println("Count by seconds");
        return LocalDateTime.now(ZoneId.systemDefault()).getLong(ChronoField.SECOND_OF_DAY);
    }
}
