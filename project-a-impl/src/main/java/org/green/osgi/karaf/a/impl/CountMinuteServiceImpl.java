package org.green.osgi.karaf.a.impl;

import org.green.osgi.karaf.a.api.CountService;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoField;

public class CountMinuteServiceImpl implements CountService {
    @Override
    public long getCount() {
        System.err.println("Count by minutes");
        return LocalDateTime.now(ZoneId.systemDefault()).getLong(ChronoField.MINUTE_OF_DAY);
    }
}
