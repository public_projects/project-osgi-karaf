package org.green.osgi.karaf.a.api;

public interface CountService {
    long getCount();
}
