/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.green.osgi.karaf.c.client;

import org.green.osgi.karaf.a.api.CountService;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.green.osgi.karaf.a.api.ServiceA;
import java.util.List;

public class Activator implements BundleActivator {

    public void start(BundleContext context) {
        try {
            List<ServiceReference<CountService>> countServiceReferences = (List<ServiceReference<CountService>>) context.getServiceReferences(CountService.class, null);
            System.err.println(countServiceReferences);

            countServiceReferences.forEach(countServiceServiceReference -> {
                System.err.println("Properties: " + countServiceServiceReference.getProperties());
                System.err.println("Count: "+ ((CountService) context.getService(countServiceServiceReference)).getCount());
            });

        } catch (InvalidSyntaxException e) {
            e.printStackTrace();
        }

        ServiceReference<ServiceA> serviceAServiceReference = (ServiceReference<ServiceA>) context.getServiceReference(ServiceA.class);
        ServiceReference<ServiceA> serviceAServiceReference2 = (ServiceReference<ServiceA>) context.getServiceReference("org.green.osgi.karaf.a.api.ServiceA");
        System.out.println("1 Starting the bundle "+ ((ServiceA) context.getService(serviceAServiceReference)).getType());
        System.out.println("2 Starting the bundle "+ ((ServiceA) context.getService(serviceAServiceReference2)).getType());
    }

    public void stop(BundleContext context) {
        System.out.println("Stopping the bundle");
    }

}
